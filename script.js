"use strict";
//Generar una contraseña (número entero aleatorio del 0 al 100)//
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
let password = getRandomInt(0, 100);
console.log(password);
//Pedir al usuario que introduzca un número dentro de ese rango.//
let numero = Number(
  prompt("Por favor introduzca un número entre el 1 y el 100")
);
//- Si el número introducido es igual a la contraseña, aparecerá en pantalla un mensaje indicando que ha ganado; si no, el mensaje indicará si la contraseña en un número mayor o menor al introducido y dará una nueva oportunidad.//
let Numtries = 5;

while (Numtries > 0) {
  prompt("Inténtalo de nuevo!");

  if (numero === password) {
    alert("¡Felicidades!¡Has ganado!");
  } else if (numero < password) {
    alert(
      "PISTA: El número que debes ingresar es menor al que has introducido"
    );
  } else if (numero > password) {
    alert(
      "PISTA: El número que debes introducir es mayor al que has introducido"
    );
  }
  Numtries--;
}
//-El usuario tendrá 5 oportunidades de acertar, si no lo consigue, aparecerá un mensaje indicando que ha perdido.//
